const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const MakeDirWebpackPlugin = require('make-dir-webpack-plugin')
const CreateFileWebpack = require('create-file-webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const isProd = process.env.NODE_ENV === 'production'

const plugins = {
  prod: [
    new MakeDirWebpackPlugin({
      dirs: [{ path: './dist/html' }]
    }),
    new CreateFileWebpack({ path: './dist/html', fileName: 'main.tpl.html', content: '' })
  ],
  dev: [
    new HtmlWebpackPlugin({ template: './index.html' }),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, '/public')
      }
    ]),
    new BundleAnalyzerPlugin({ analyzerMode: 'server' }, true)
  ]
}

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, './dist')
  },
  stats: { children: false },
  optimization: {
    minimize: true
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        enforce: 'pre',
        loader: 'eslint-loader',
        exclude: /node_modules/,
        options: {
          emitWarning: true,
          configFile: './.eslintrc.json'
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it use publicPath in webpackOptions.output
              publicPath: './public'
            }
          },
          'css-loader'
        ]
      },
      {
        test: /\.(svg|png|jpg|gif)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'images/'
            }
          }
        ]
      },
      {
        test: /\.(woff(2)?|otf||ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      },
      {
        type: 'javascript/auto',
        test: /\.json$/,
        use: ['file-loader'],
        include: /\.\/manifest/ // for e.g, but better to only copy particular JSON files (not all)
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'assets/js/bundle.js'
  },

  plugins: [
    ...(isProd ? plugins.prod : plugins.dev),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name].css',
      chunkFilename: '[id].css'
    })
  ],
  devServer: {
    stats: {
      modules: false,
      children: false,
      source: false
    }
  }
}
