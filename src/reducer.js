export const isInsideDashboard = !!(window.frameElement && window.frameElement.getAttribute('data-preview'))

const getEnvData = () => {
	if (process.env.NODE_ENV !== 'production') {
		return require('../api/sample-data.json')
	}

	return window.__IWDATA__
}

export const initialState = getEnvData()

export default (state = initialState, action) => {
	switch (action.type) {
		case '__dashboardLivePreviewChange':
			/**
			 * v1 Manifest templates (i.e EventsApp) have different requirements than v2/v3.
			 * Use the payloadToValidState helper as a starting point for working with v1 data.
			 *
			 * _example_:
			 * const changes = payloadToValidState(action)
			 * return { ...state, ...changes }
			 *
			 */

			return { ...state, ...(action.payload || action) }
		default:
			return state
	}
}
