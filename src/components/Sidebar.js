import React from 'react'
import styled from 'styled-components'
import axios from 'axios'

import { Button } from './'
import { playIcon } from '../assets/icons'

const Template = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`
const Top = styled.div`
  height: 80%;
  position: relative;
  background-image: ${({ theme }) => `url(${theme?.images?.background})`}};
  background-position: center;
  background-size: cover;
`

const Buttons = styled.div`
  position: absolute;
  top: 3vh;
  width: 100%;
  height: 8vh;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`

const Separator = styled.div`
    position: absolute;
    bottom: 0;
    height: 20%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;

    .wave {
        background-color: rgba(0, 0, 0, 0.0)
        position: relative;
    }
    .wave::before,
    .wave::after {
        border-bottom: ${({ theme }) => `5px solid ${theme?.colors?.logoBackgroundColor}`};
    }
    .wave::before {
        content: "";
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        height: 10px;
        background-size: 20px 40px;
        background-image: ${({ theme }) => `radial-gradient(
            circle at 10px -15px,
            transparent 20px,
            ${theme?.colors?.logoBackgroundColor} 21px
        )`};
    }
    .wave::after {
        content: "";
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        height: 15px;
        background-size: 40px 40px;
        background-image: ${({ theme }) => `radial-gradient(
            circle at 10px 26px,
            ${theme?.colors?.logoBackgroundColor} 20px,
            transparent 21px
        )`};
    }
`

const Bottom = styled.div`
  height: 20%;
  display: flex;
  justify-content: center;
  align-items: center;
`

const BackButtonInner = styled.div`
  display: flow;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

const Flipped = styled.div`
  transform: scale(-1, 1);
  height: 100%;
  display: flex;
  flex-direction: column;
  margin-right: 0.25em;

  img {
    height: 1.2em;
    width: auto;
  }
`

export default ({ logo, selectedCategory, selectedCampaign, goBack, data, setToastText }) => {
  const handleResume = () => {
    const { cchd_account, cchd_zone, cchd_device_id, cchd_pin } = data
    const url = `https://cchdrsrc.channelshd.com/crs/crsraw.php?auth=${cchd_pin}&stationid=${cchd_account}-${cchd_zone}-${cchd_device_id}&command=RESUME&param=${selectedCampaign}`

    axios
      .post(url)
      .then(response => {
        console.log(response)
        setToastText(`Resuming`)
      })
      .catch(error => {
        console.error(error)
        setToastText(error.message)
      })
  }

  return (
    <Template>
      <Top>
        <Buttons>
          <Button text="RESUME" inverted small action={handleResume} />
          <Button
            text={
              <BackButtonInner>
                <Flipped>
                  <img src={playIcon} />
                </Flipped>
                <span>BACK</span>
              </BackButtonInner>
            }
            inverted
            small
            disabled={!selectedCategory}
            action={goBack}
          />
        </Buttons>
        <Separator>
          <div className="wave"></div>
        </Separator>
      </Top>
      <Bottom>
        <img src={logo} />
      </Bottom>
    </Template>
  )
}
