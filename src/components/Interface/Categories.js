import React from "react"
import styled from "styled-components"
import { Button } from "../"

const Template = styled.div`
    width: 100%;
    height: 100%;
`
const HeaderContainer = styled.div`
    height: 34%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    align-items: center;

    div {
        text-align: center;
    }

    h1 {
        font-size: 2.6em;
        font-family: "Century Gothic-Bold";
    }

    em {
        font-family: "Century Gothic-Italic";
        display: inline-block;
        margin: 1em;
    }
`

const CategoriesContainer = styled.div`
    height: 69vh;
    width: 100%;
    padding: 0 10vw 0 10vw;
    overflow: scroll;
`

export default ({ categories, selectCategory }) => {
    if (!categories) return null
    return (
        <Template>
            <HeaderContainer>
                <div>
                    <h1>SELECT A</h1>
                    <h1>CATEGORY</h1>

                    <em>Content will appear on mounted screens ahead</em>
                </div>
            </HeaderContainer>
            <CategoriesContainer>
                {categories.map((c, i) => {
                    return (
                        <Button
                            key={i}
                            text={c.name}
                            action={() => {
                                selectCategory(c)
                            }}
                            style={{ width: "100%" }}
                            center
                        />
                    )
                })}
            </CategoriesContainer>
        </Template>
    )
}
