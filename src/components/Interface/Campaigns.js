import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import { Button } from '../'

const Template = styled.div`
  position: absolute;
  top: 0;
  width: 90%;
  left: 5%;
  height: 100%;
`

const Top = styled.div`
  position: absolute;
  top: 3vh;
  width: 100%;
  height: 8vh;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`

const CampaignsContainer = styled.div`
  position: absolute;
  top: 13vh;
  width: 100%;
  height: 90vh;
  overflow: scroll;
`

const Title = styled.h1`
  font-size: 1.6em;
  letter-spacing: 0.1em;
  font-family: 'Century Gothic-Bold';
`

const ButtonInner = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  img {
    height: 3em;
    width: auto;
  }
`

export default ({ data, selectedCategory, setSelectedCampaign, setToastText }) => {
  const { campaigns } = data
  const [campaignOptions, setCampaignOptions] = useState([])

  useEffect(() => {
    if (!selectedCategory) setCampaignOptions([])
    else {
      const newCampaignOptions = []
      selectedCategory.campaigns.forEach(c => {
        newCampaignOptions.push(campaigns[c])
      })
      setCampaignOptions(newCampaignOptions)
    }
  }, [campaigns, selectedCategory])

  const handleCampaignSelection = c => {
    const { campaign: campaign, name: campaignName } = c
    const { cchd_account, cchd_device_id, cchd_pin, cchd_zone } = data

    setSelectedCampaign(campaign)

    const url = `https://cchdrsrc.channelshd.com/crs/crsraw.php?auth=${cchd_pin}&stationid=${cchd_account}-${cchd_zone}-${cchd_device_id}&command=PLAYCAMPAIGN&param=${campaign}`

    axios
      .post(url)
      .then(response => {
        console.log(response)
        setToastText(`Playing ${campaignName}`)
      })
      .catch(error => {
        console.error(error)
        setToastText(error.message)
      })
  }

  const renderButton = (campaign, index) => {
    const inner = (
      <ButtonInner>
        <span>{campaign.name}</span>
        {campaign.thumbnail && <img src={campaign.thumbnail} />}
      </ButtonInner>
    )

    return (
      <Button key={index} text={inner} style={{ width: '100%' }} action={() => handleCampaignSelection(campaign)} />
    )
  }

  if (!selectedCategory) return null

  return (
    <Template>
      <Top>
        <Title>{selectedCategory.name}</Title>
      </Top>
      <CampaignsContainer>
        {campaignOptions.map((c, i) => {
          return renderButton(c, i)
        })}
      </CampaignsContainer>
    </Template>
  )
}
