import React, { useState, useEffect } from "react"
import styled from "styled-components"

const TOAST_LIFESPAN = 5.0
const FADE_EFFECT_DURATION = 0.25

const Template = styled.div`
    position: absolute;
    top: 5vh;
    left: 50%;
    transform: translateX(-50%);
    z-index: 2;
    color: ${({ theme }) => theme?.colors?.notificationTextColor};
    background-color: ${({ theme }) =>
        theme?.colors?.notificationBackgroundColor};
    font-family: "Century Gothic-Italic";
    padding: 1em;
    border-radius: 0.5em;
    opacity: ${props => (props.visible ? "1.0" : "0.0")};
    transition: opacity ${FADE_EFFECT_DURATION}s;
    cursor: pointer;
`

export default ({ text, setText }) => {
    const [isVisible, setIsVisible] = useState(false)

    const hideToast = () => {
        setIsVisible(false)
    }

    useEffect(() => {
        let timeout
        if (!isVisible) {
            timeout = setTimeout(() => {
                setText(null)
            }, FADE_EFFECT_DURATION * 1000)
        }

        return () => clearTimeout(timeout)
    }, [isVisible, setText])

    useEffect(() => {
        let timeout
        if (text) {
            setIsVisible(true)
            timeout = setTimeout(() => {
                hideToast()
            }, TOAST_LIFESPAN * 1000)
        }

        return () => clearTimeout(timeout)
    }, [text])

    return (
        <Template visible={isVisible} onClick={hideToast}>
            <span>{text}</span>
        </Template>
    )
}
