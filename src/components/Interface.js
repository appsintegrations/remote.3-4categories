import React, { useState } from 'react'
import styled, { css, keyframes } from 'styled-components'
import { Campaigns, Categories } from './Interface/'

const TRANSITION_EFFECT_DURATION = 0.3

const fadeOut = keyframes`
    from {
        opacity: 1.0;
    }
    
    to {
        opacity: 0.0;
    }
`

const Template = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`

const MenuWrapper = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  animation: ${props =>
    props.fadeOut
      ? css`
          ${fadeOut} ${TRANSITION_EFFECT_DURATION + 0.05}s
        `
      : null};
  z-index: ${props => (props.top ? '1' : '0')};
  opacity: ${props => (props.top ? '1.0' : '0.0')};
`

export default ({ data, selectedCategory, selectCategory, setSelectedCampaign, isTransitioning, setToastText }) => {
  const { categories } = data
  return (
    <Template>
      <MenuWrapper fadeOut={isTransitioning && !selectedCategory} top={!selectedCategory}>
        <Categories categories={categories} selectCategory={selectCategory} />
      </MenuWrapper>

      <MenuWrapper fadeOut={isTransitioning && selectedCategory} top={selectedCategory}>
        <Campaigns
          data={data}
          selectedCategory={selectedCategory}
          setSelectedCampaign={setSelectedCampaign}
          setToastText={setToastText}
        />
      </MenuWrapper>
    </Template>
  )
}
