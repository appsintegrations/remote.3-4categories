import React from "react"
import styled from "styled-components"

const Template = styled.div`
    color: ${({ theme, inverted }) => {
        return inverted ? theme?.colors?.buttonColor : theme?.colors?.textColor
    }};
    background-color: ${({ theme, inverted }) => {
        return inverted ? theme?.colors?.textColor : theme?.colors?.buttonColor
    }};
    display: flex;
    flex-direction: row;
    justify-content: ${props => (props.center ? "center" : "space-between")};
    align-items: center;
    min-height: ${props => (props.small ? "1.5em" : "3.5em")};
    margin: 0.75em 0 0.75em;
    padding: 0.25em 0.75em 0.25em 0.75em;
    border-radius: 0.5em;
    cursor: pointer;
    font-family: "Century Gothic-Bold";
    font-size: 1.5em;
    text-align: center;
    white-space: ${props => (props.small ? "nowrap" : "normal")};
    opacity: ${props => (props.disabled ? "0.5" : "1.0")};
    
    div.content-wrapper {
        width: 100%;
    }
`

export default ({ text, action, inverted, style, small, disabled, center }) => {
    return (
        <Template
            onClick={disabled ? () => {} : action}
            inverted={inverted}
            style={{ ...style }}
            small={small}
            disabled={disabled}
            center={center}
        >
            <div className="content-wrapper">{text}</div>
        </Template>
    )
}
