import React, { useEffect, createContext, useReducer } from "react"

import reducer, { initialState } from "./reducer"

export const DataContext = createContext(initialState)
export const DataConsumer = DataContext.Consumer

export const useApp = () => React.useContext(DataContext)

const DataProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    useEffect(() => {
        const handleLivePreview = ({ data }) => dispatch(data)

        window.addEventListener("message", handleLivePreview)

        return () => {
            window.removeEventListener("message", handleLivePreview)
        }
    }, [])

    return <DataContext.Provider value={state}>{children}</DataContext.Provider>
}

export default DataProvider
