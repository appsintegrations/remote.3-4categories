import React from "react"
import { ThemeProvider } from "styled-components"
import { useApp } from "./DataProvider"
import App from "./App"

const Container = () => {
    const { data, settings, slide } = useApp()

    return (
        <ThemeProvider theme={settings || {}}>
            <App data={data} settings={settings} slide={slide} />
        </ThemeProvider>
    )
}

export default Container
