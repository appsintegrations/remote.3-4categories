import React, { useState } from 'react'
import styled from 'styled-components'

import { Interface, Sidebar, Toast } from './components'

const TRANSITION_EFFECT_DURATION = 0.3

const Template = styled.div`
  position: relative;
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: row;
  color: ${({ theme }) => theme?.colors?.textColor};
  overflow: hidden;
`

const Left = styled.div`
  width: 35%;
  height: 100vh;
  background-color: ${({ theme }) => theme?.colors?.logoBackgroundColor};
  overflow: hidden;
`

const Right = styled.div`
  width: 65%;
  height: 100vh;
  background-color: ${({ theme }) => theme?.colors?.backgroundColor};
  overflow: hidden;
`

const App = ({ data, settings }) => {
  const [selectedCategory, setSelectedCategory] = useState(null)
  const [selectedCampaign, setSelectedCampaign] = useState(null)
  const [isTransitioning, setIsTransitioning] = useState(false)
  const [toastText, setToastText] = useState(null)

  const selectCategory = category => {
    setIsTransitioning(true)
    setTimeout(() => {
      setIsTransitioning(false)
      setSelectedCategory(category)
    }, TRANSITION_EFFECT_DURATION * 1000)
  }

  return (
    <React.Fragment>
      <Toast text={toastText} setText={setToastText} />
      <Template>
        <Left>
          <Sidebar
            data={data}
            logo={settings?.images?.logo}
            selectedCategory={selectedCategory}
            goBack={() => selectCategory(null)}
            setToastText={setToastText}
            selectedCampaign={selectedCampaign}
          />
        </Left>
        <Right>
          <Interface
            data={data}
            selectedCategory={selectedCategory}
            selectCategory={selectCategory}
            setSelectedCampaign={setSelectedCampaign}
            isTransitioning={isTransitioning}
            setToastText={setToastText}
          />
        </Right>
      </Template>
    </React.Fragment>
  )
}

export default App
